#include <dataStruct/Array.hxx>
#include <alg/Alg.h>

/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
template<class T>
void swap(T &t, T &t2) {
    T temp = t;
    t = t2;
    t2 = temp;
}

template<class T>
int compare(T &a, T &b) {
    if (a < b) return -1;
    else if (a > b) return 1;
    else return 0;
}

template<class T>
int partition(T *t, int l, int r) {
    auto pivot = t[(l + r) / 2];
//                std::cout<<"Pivot = " << pivot<<'\n';
    int i = l - 1;
    int j = r;
    while (i <= j) {
        while (compare(pivot, t[++i]) < 0)if (i == r)break;
        while (compare(pivot, t[--j]) > 0)if (j == l) break;
        if (i <= j) {
            swap(t[i], t[j]);
            i++;
            j--;
        }
//        if(i>=j) break;
//        swap(t[i],t[j]);
    }
//    swap(t[l],t[j]);
    return j;
}

template<class T>
void print(T &t, size_t l, size_t r) {
    using std::cout, std::endl;
    for (size_t i = l; i < r; ++i) {
//        if((i-l)%6==0) std::cout<<'\n';
//        std::cout<<t[i]<<'\t';
        cout << t[i] << endl;
    }
    cout << '\n';
}

void quickSort(int *arr, int left, int right) {
    int i = left, j = right;
    int tmp;
    int pivot = arr[(left + right) / 2];

    /* partition */
    while (i <= j) {
        while (arr[i] < pivot)
            i++;
        while (arr[j] > pivot)
            j--;
        if (i <= j) {
            tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
            i++;
            j--;
        }
    };

    /* recursion */
    if (left < j)
        quickSort(arr, left, j);
    if (i < right)
        quickSort(arr, i, right);

}

template<typename T>
void quicksort(T *t, int l, int r) {
    if (r <= l) return;

    int i = partition(t, l, r);
    quicksort<T>(t, l, i - 1);
    quicksort<T>(t, i + 1, r);

}

int main(int argc, char **args) {
//    const cw::Array<int> &array = cw::Array<int>(5);
//    array[0] = 4;
//    array[1] = 4;
//    array[2] = 3;
//    array[3] = 2;
//    array[4] = 1;
    int sz = 300;
    int *ar = new int[sz];
    const cw::Array<int> array(sz);
    for (int i = 0; i < sz; ++i) {
        array[i] = sz - i;
        ar[i] = sz - i;
    }
//    print(ar,0,sz);

    std::cout << array;
    print<int *>(ar, 0, sz);
    std::cout << '\n';
    cw::sort::quickSort<int *>(ar, 0, sz, cw::sort::helper::compare<int>);
    std::cout << std::endl;
//    print(ar,0,sz);
//    cw::sort::quickSort<int *>(ar, 0,sz,cw::sort::helper::reverseCompare<int>);
    cw::sort::quickSort(array);
    print<int *>(ar, 0, sz);
    std::cout << array;
//    int i1 = 1;
//    int i2 = 1;
//
//    std::cout<<((i1<i2)^false)<< ' ';
//    std::cout<<((i1<i2)^true)<< ' ';
//    std::cout<<'\n';
//    std::cout<<((i1>i2)^false)<< ' ';
//    std::cout<<((i1>i2)^true)<< ' ';
//    std::cout<<'\n';
//    std::cout<<'\n';
//
//    std::cout<<(!((i1<i2)^false)) << ' ';
//    std::cout<<(!((i1<i2)^true)) << ' ';
//    std::cout<<'\n';
//    std::cout<<(!((i1>i2)^false)) << ' ';
//    std::cout<<(!((i1>i2)^true)) << ' ';
//    cw::Array<int> array(30);
//    for (int i = 0; i < (int)array.arraySize(); ++i) {
//        array[i] = i;
//    }
//
//    std::cout<<array;
//
//    cw::sort::quickSort<cw::Array<int>>(array, 0, static_cast<int>(array.arraySize()), cw::sort::helper::compare<int>);
//
//
//    std::cout<<array;


    return 0;
}