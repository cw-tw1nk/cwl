#ifndef CPP_OOP_1_KRLV_HXX
#define CPP_OOP_1_KRLV_HXX


#include <iostream>
#include <cmath>

void random(const SqMatrix &s, int pr) {
    for (int i = 0; i < N; ++i) {
        if (i == pr) A[i][0] = 1;
        else
            A[i][0] = 0;
    }
}

void dop(const AMatrix &m, int k) const {
    for (int i = 0; i < N; ++i) {
        A[k][i] = m.A[i][0];
    }
}

using namespace std;
double eps = -5e;

void main() {
    int n;
    cout << "Введите размерность матрицы: ";
    cin >> n;
    SqMatrix A(n - 1, 3.58); // организовать  ввод с файла
    SqMatrix CC(n - 1);// матрица, состоящая из векторов до (n-1)
    AMatrix CN(1, n - 1); //создание матрицы,состоящий из одного столбца
    int pr = 0; //будет изменяться, если полученная мат-ца СС будет  линейнонезависимой
    for (int i = 0; i < n; ++i) // заполнение мат-цы СС
    {
        if (i == 0) random(CC, pr); // вектор с(ноль) p заполнится необходимым способом
        else CN *= A;
        CC.dop(CN, i); // вектор заполнит необходимый столбец	мат-цы СС
        if ((i == n - 1) && (fabs(CC.opr) < eps)) {
            ++pr;
            i = 0;
        }
    }
    CN *= A;


}


#endif //CPP_OOP_1_KRLV_HXX
