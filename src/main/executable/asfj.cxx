/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_1_EX_HXX
#define CPP_OOP_1_EX_HXX

#include "anna/Algmatrix.h"
#include <anna/SqMatrix.hxx>

int main() {
    AMatrix A(2, 3, 1);
    AMatrix B(3, 2, 2);
    cout << A * B;
    SqMatrix
    C(2, 3);

    return 0;
}


#endif //CPP_OOP_1_EX_HXX
