/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <cstddef>
#include <iostream>
#include <dataStruct/Stack.hxx>
namespace cw {
#ifdef __ssize_t_defined

#include <sys/types.h>

    typedef ssize_t size;
#else
#include <cstddef>
    typedef ptrdiff_t arraySize
#endif
    template<class T>
    void swap(T &t, T &t2) {
//        std::cout<<"swapped: " << t << " and " << t2<< '\n';
        T temp = t;
        t = t2;
        t2 = temp;
    }

    template<class T>
    void arithmeticSwap(const T &t, size_t index1, size_t index2) {
        t[index1] = t[index1] + t[index2];
        t[index2] = t[index1] - t[index2];
        t[index1] = t[index1] - t[index2];
    }

    namespace sort {
        namespace helper {
            inline void push2(cw::Stack<int> &stack, int first, int second) {
                stack.push(second).push(first);
            }
            template<class T>
            int compare(const T &a, const T &b) {
//                int result = (a<b)?(-1):(a>b)?1:0;
//                std::cout<<"compare: " << a << " and " << b << ", result: " << result << '\n';
                if (a < b) return -1;
                else if (a > b) return 1;
                else return 0;
            }

            template<class T>
            int reverseCompare(T &a, T &b) {
                int result = (a < b) ? (1) : (a > b) ? -1 : 0;
                std::cout << "compare: " << a << " and " << b << ", result: " << result << '\n';
                if (a < b) return 1;
                else if (a > b) return -1;
                else return 0;
            }

            template<class T>
            void shift(const T &t, size_t el, bool type) {
                for (size_t i = el; i >= 1; --i) {
                    if ((t[i] > t[i - 1]) ^ type) return;
                    else swap(t[i], t[i - 1]);
                }
            }

            template<class T, class E>
            void shift(const T &t, size_t el, int (*compare)(const E &, const E &) = cw::sort::helper::compare<E>) {
                for (size_t i = el; i >= 1; --i) {
                    if (compare(t[i], t[i - 1]) >= 0) return;
                    else swap(t[i], t[i - 1]);
                }
            }

            template<class T>
            size_t select(T &t, size_t startIndex, bool type = false) {
                size_t size = t.size();
                size_t index = startIndex;
                auto max = t[startIndex];

                for (size_t i = startIndex; i < size; ++i) {
                    if ((max > t[i]) ^ type) {
                        max = t[i];
                        index = i;
                    }
                }
                return index;
            }

            template<class T>
            void print(T &t, size_t l, size_t r) {
                using std::cout, std::endl;
                for (size_t i = l; i < r - 1; ++i) {
//        if((i-l)%6==0) std::cout<<'\n';
//        std::cout<<t[i]<<'\t';
                    cout << t[i] << endl;
                }
                cout << '\n';
            }

            template<class T, class E>
            E selectPivot(const T &array, int l, int r, int (*compare)(const E &, const E &)) {
                E lElem = array[l];
                E cElem = array[(l + r) / 2];
                E rElem = array[r];
                if (compare(lElem, cElem) >= 0 || compare(lElem, rElem) <= 0) {
                    if (compare(cElem, rElem) >= 0) return rElem;
                    else return cElem;
                } else return lElem;
            }

            template<class T, class E>
            int partition(const T &array, int l, int r, int (*compare)(const E &, const E &)) {
//                auto pivot = array[r];
                E pivot = selectPivot(array, l, r, compare);
                int i = l;
                int j = r;
                while (true) {
                    while (compare(array[i], pivot) < 0)++i;
                    while (compare(array[j], pivot) > 0)--j;
                    if (i >= j) return j;
                    swap<E>(array[i], array[j]);
                }
            }
        }

        template<class T>
        void bubbleSortWithFlag(const T &array, bool type = false) {
            size_t sz = array.size();
            bool flag = true;
            while (flag) {
                flag = false;
                for (size_t i = 0; i < sz - 1; ++i) {
                    if ((array[i] > array[i + 1]) ^ type) {
                        cw::swap(array[i], array[i + 1]);
                        flag = true;
                    }
                }
            }
        }

        template<class T>
        void bubbleSort(const T &array, bool type = false) {
            size_t sz = array.size();
            for (size_t i = 0; i < sz; ++i) {
                for (size_t j = 0; j < sz - 1; ++j) {
                    if ((array[j] > array[j + 1]) ^ type) {
                        cw::swap(array[j], array[j + 1]);
                    }
                }
            }
        }

        template<class T>
        void insertionSort(const T &array, bool type = false) {
            size_t size = array.size();
            for (size_t i = 0; i < size - 1; ++i) {
                if ((array[i] > array[i + 1]) ^ type) {
                    cw::sort::helper::shift(array, i + 1, type);
                }
            }
        }

        template<class T, class E>
        void insertionSort(const T &array, int start, int end,
                           int (*compare)(const E &, const E &) = cw::sort::helper::compare<E>) {
            for (int i = start; i < end - 1; ++i) {
                if ((array[i] > array[i + 1])) {
                    cw::sort::helper::shift(array, static_cast<size_t>(i + 1), compare);
                }
            }
        }

        template<class T>
        void selectionSort(const T &array, bool type = false) {
            size_t size = array.size();
            for (size_t i = 0; i < size; ++i) {
                size_t i1 = cw::sort::helper::select(array, i, type);
                cw::swap(array[i], array[i1]);
            }
        }

        template<class T, class E>
        void
        quickSort(const T &array, int l, int r, int (*compare)(const E &, const E &) = cw::sort::helper::compare<E>) {
            if (l >= r)
                return;
//            if((r-l)<10) {
//                insertionSort<T>(array,l,r+1,compare);
//                return;
//            }
            int i = cw::sort::helper::partition<T, E>(array, l, r, compare);
            quickSort<T, E>(array, l, i - 1, compare);
            quickSort<T, E>(array, i + 1, r, compare);


        }

        template<class T, class E>
        void quickSortWithStack(const T &array, int l, int r, int m,
                                int (*compare)(const E &, const E &) = cw::sort::helper::compare<E>) {
            cw::Stack<int> stack;
            cw::sort::helper::push2(stack, l, r);
            while (!stack.isEmpty()) {
                l = stack.pop();
                r = stack.pop();
                if ((r - l) <= m) {
                    insertionSort(array, l, r, compare);
                    continue;
                }
                int i = cw::sort::helper::partition<T, E>(array, l, r, compare);
                if (i - 1 > r - i) {
                    cw::sort::helper::push2(stack, l, i - 1);
                    cw::sort::helper::push2(stack, i + 1, r);
                } else {
                    cw::sort::helper::push2(stack, i + 1, r);
                    cw::sort::helper::push2(stack, l, i - 1);
                }
            }
        }

        template<class T, class E>
        void quickSortWithStack(const T &array, int l, int r,
                                int (*compare)(const E &, const E &) = cw::sort::helper::compare<E>) {
            quickSortWithStack<T, E>(array, l, r, 1, compare);
        }

        template<class T, class E>
        void hybridSortWithStack(const T &array, int l, int r,
                                 int (*compare)(const E &, const E &) = cw::sort::helper::compare<E>) {
            quickSortWithStack<T, E>(array, l, r, 10, compare);
        }

        template<class T>
        void quickSort(const T &array, bool type = false) {
            if (type) {
                cw::sort::quickSort<T, decltype(array[0])>(array, 0, array.size(),
                                                           cw::sort::helper::compare<decltype(array[0])>);
            } else {
                cw::sort::quickSort<T, decltype(array[0])>(array, 0, array.size(),
                                                           cw::sort::helper::reverseCompare<decltype(array[0])>);
            }
        }

    }


}
