/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_1_DENODE_HXX
#define CPP_OOP_1_DENODE_HXX
namespace cw {
    template<class T>
    class DeNode {
        T *next;
        T *previous;

        T item;

        DeNode(const T &Item) : next(nullptr), previous(nullptr), item(Item) {}

        DeNode(const T *next, T Item) : next(next), previous(nullptr), item(Item) {}

        DeNode(const T *previous, T Item) : next(nullptr), previous(previous), item(Item) {}

        DeNode(const T *next, const T *previous, T Item) : next(next), previous(previous), item(Item) {}

        ~DeNode() {
            next = nullptr;
            previous = nullptr;
        }

    public:
        T get() const {
            return item;
        }

        void set(T item) {
            DeNode::item = item;
        }

        T *getNext() const {
            return next;
        }

        void setNext(T *next) {
            DeNode::next = next;
        }

        T *getPrevious() const {
            return previous;
        }

        void setPrevious(T *previous) {
            DeNode::previous = previous;
        }
    };
}

#endif //CPP_OOP_1_DENODE_HXX
