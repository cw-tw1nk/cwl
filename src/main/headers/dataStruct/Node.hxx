/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_1_NODE_HXX
#define CPP_OOP_1_NODE_HXX

#include <ostream>

namespace cw {
    template<class T>
    class Node {
        Node *pNode = nullptr;
        T value;
    public:
        T get() {
            return value;
        }

        Node &set(const T &t) {
            value = t;
            return *this;
        }

        void setPNode(Node<T> *pNode) {
            Node::pNode = pNode;
        }

        void setValue(T value) {
            Node::value = value;
        }

        Node(T &value) : value(value) {}

        Node(Node<T> *pNode, T value) : pNode(pNode), value(value) {}

        Node *getPNode() const {
            return pNode;
        }

        virtual ~Node() {
            pNode = nullptr;
        }

        friend std::ostream &operator<<(std::ostream &os, const Node &node) {
            os << node.value;
            return os;
        }
    };
}

#endif //CPP_OOP_1_NODE_HXX
