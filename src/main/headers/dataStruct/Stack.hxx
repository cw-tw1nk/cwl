/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_1_STACK_HXX
#define CPP_OOP_1_STACK_HXX

#include <stdexcept>
#include <exception>
#include <ostream>
#include "Node.hxx"

namespace cw {
    template<class T>
    class Stack {
        Node<T> *currentNode;
    public:
        Stack() : currentNode(nullptr) {}

        Stack(Stack &stack) {
            if (stack.currentNode != nullptr) {
                Stack tmp;
                Node<T> *node = stack.currentNode;
                while (node != nullptr) {
                    tmp.push(node->get());
                    node = node->getPNode();
                }
                node = tmp.currentNode;
                while (node != nullptr) {
                    push(node->get());
                    node = node->getPNode();
                }
            }
        }

        Stack &push(const T &t) {
            currentNode = new Node<T>(currentNode, t);
            return *this;
        }

        T pop() {
            Node<T> *node = currentNode;
            T value = (*currentNode).get();
            currentNode = (*currentNode).getPNode();
            delete node;
            return value;
        }

        T peek() {
            return currentNode->get();
        }

        bool isEmpty() {
            return currentNode == nullptr;
        }

        int search(T &value) {
            Node<T> *node = currentNode;
            for (int i = 0; node != nullptr; ++i) {
                if (node->get() == value) return i;
            }
            return -1;
        }

        friend std::ostream &operator<<(std::ostream &os, const Stack &stack) {
            if (stack.currentNode != nullptr) {
                Node<T> *node = stack.currentNode;
                while (node != nullptr) {
                    os << *node;
                    node = node->getPNode();
                }

            }
            return os;
        }

        virtual ~Stack() {

            Node<T> *node = currentNode;
            while (currentNode != nullptr) {
                currentNode = (*currentNode).getPNode();
                delete node;
            }
        }
    };
}

#endif //CPP_OOP_1_STACK_HXX
