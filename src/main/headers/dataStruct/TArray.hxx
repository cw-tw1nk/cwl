/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_1_TARRAY_HXX
#define CPP_OOP_1_TARRAY_HXX

#include <new>
#include <utils/Random.hxx>
#include <ostream>

namespace cw {
    template<class T>
    class TArray {
        T *array = nullptr;
        size_t arraySize = 0;
    public:
        /**
         * Create empty array
         */
        TArray() {}

        /**
         * Create array
         * @param size
         */
        TArray(size_t size) : arraySize(size) {
            if (size < 0) throw std::bad_alloc();
            else if (size > 0) array = new T[size];
        }

        /**
         * Create array with default value
         * @param size
         * @param value default value
         */
        TArray(size_t size, const T &value) : arraySize(size) {
            if (size < 0) throw std::bad_alloc();
            else if (size > 0) {
                array = new T[size];
                for (size_t i = 0; i < size; ++i) {
                    array[i] = value;
                }
            }
        }

        /**
         * Create copy array
         * @param copy
         */
        TArray(const TArray<T> &copy) : arraySize(copy.arraySize) {
            if (arraySize != 0) {
                array = new T[arraySize];
                for (size_t i = 0; i < arraySize; ++i) {
                    this->array[i] = copy.array[i];
                }
            }
        }

        T &operator[](size_t index) const {
            return array[index];
        }

        bool operator==(const TArray &other) const {
            if (other.arraySize != arraySize) return false;
            for (size_t i = 0; i < arraySize; ++i) {
                if (other.array[i] != this->array[i]) return false;
            }
            return true;
        }

        bool operator!=(const TArray &other) const {
            return !(operator==(other));
        }

        size_t size() const {
            return arraySize;
        }

        TArray<T> &operator=(const TArray &other) {
            if (other.arraySize != 0) {
                arraySize = other.arraySize;
                delete[] array;
                return TArray(other);
            }
        }

        void swap(size_t pos1, size_t pos2) const {
            auto buffer = array[pos1];
            array[pos1] = array[pos2];
            array[pos2] = buffer;
        }

        void shuffle(size_t (*random)(size_t, size_t) = cw::random::lcg::rand) const {
            for (size_t i = 0; i < arraySize; ++i) {
                size_t pos2 = random(i, arraySize - 1);
                swap(i, pos2);
            }
        }

        static const TArray seq(size_t size) {
            TArray tArray(size);
            for (size_t i = 0; i < size; ++i) {
                tArray[i] = static_cast<T>(i);
            }
            return tArray;
        }

        friend std::ostream &operator<<(std::ostream &os, const TArray &array) {
            for (size_t i = 0; i < array.arraySize; ++i) {
                os << array.array[i];
                if (i % 10 == 9) os << '\n';
                else os << ' ';
            }
            return os;
        }
    };
}

#endif //CPP_OOP_1_TARRAY_HXX
