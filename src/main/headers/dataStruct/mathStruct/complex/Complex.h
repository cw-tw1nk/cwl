/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_1_COMPLEX_H
#define CPP_OOP_1_COMPLEX_H

#include <iostream>
/**
 * @class cw::Complex
 * @brief Complex number class
 * @details Class for working with complex numbers
 * @author Allex Atamanenko
 */
namespace cw {
    class Complex {
    private:
        double Real, Image;
    public:
        /**
         * @brief Default constructor that initializes fields with zeros
         * @return Complex
         */
        Complex();

        /**
         * @brief Constructor that initializes fields with Re, Im
         * @param Re
         * @param Im
         * @return Complex
         */
        Complex(double Re, double Im);

        /**
         * @brief Getter for field Real
         * @return double
         */
        double getRe() const;

        /**
         * @brief Setter for field Real
         * @return void
         */
        void setRe(double Re);

        /**
         * @brief geter for field Image
         * @return double
         */
        double getIm() const;

        /**
         * @brief seter for field real
         * @return void
         */
        void setIm(double Im);

        /**
         * @brief Calc module complex number
         * @return double
         */
        const double module() const;

        /**
         * @brief Calc angle complex number
         * @return double
         */
        const double angle() const;

        /**
         * @brief Output trigonometric form complex number
         * @return const Complex
         */
        const Complex outputTrigonometricForm() const;

        /**
         * @brief Output exponential form complex number
         * @return Complex
         */
        const Complex outputExponentialForm() const;

        /**
         * @brief Computes the complex root of the nth power of a complex number
         * @param n
         * @return Array of n complex numbers
         */
        Complex *root(unsigned n) const;

        /**
         * @brief Calculate complex Ln
         * @param n
         * @return Complex
         */
        const Complex Ln(int n) const;

        /**
         * @brief Raising the complex number to a complex power on the branch n
         * @param b
         * @param n
         * @return Complex
         */
        const Complex pow(const Complex &b, int n) const;

        const Complex operator+(const Complex &c) const;

        const Complex operator-(const Complex &c) const;

        const Complex operator*(const Complex &c) const;

        const Complex operator/(const Complex &c) const;

        const Complex &operator+() const;

        Complex operator-() const;


        Complex &operator+=(const Complex &c);

        Complex &operator-=(const Complex &c);

        Complex &operator*=(const Complex &c);

        Complex &operator/=(const Complex &c);

        Complex &operator+=(double Re);

        Complex &operator-=(double Re);

        Complex &operator*=(double Re);

        Complex &operator/=(double Re);

        /**
         * @brief Output the algebraic form of a complex number
         * @param os
         * @param complex
         * @return std::ostream
         */
        friend std::ostream &operator<<(std::ostream &os, const Complex &complex);

        friend std::istream &operator>>(std::istream &is, Complex &complex);

        bool operator==(const Complex &rhs) const;

        bool operator!=(const Complex &rhs) const;

        friend const Complex operator+(double Re, const Complex &c);

        const Complex operator+(double Re) const;

        friend const Complex operator-(double Re, const Complex &c);

        const Complex operator-(double Re) const;

        friend const Complex operator*(double Re, const Complex &);

        friend const Complex operator/(double Re, const Complex &);

        const Complex operator*(double Re) const;

        const Complex operator/(double Re) const;

        /**
         * @brief Raises a complex number to a power
         * @param n
         * @return Complex
         */
        const Complex operator^(unsigned n);

        /**
         * @brief Raises a complex number to a complex power on the main branch
         * @param b
         * @return Complex
         */
        const Complex operator^(const Complex &b);

        /**
         * @brief Сomputes the complex root of the nth power of a real number
         * @param a
         * @param n
         * @return Complex
         */
        static Complex *root(double a, int n);

        /**
         * binary number construction to the power n
         * @param a
         * @param n
         * @return double
         */
        static double pow(double a, unsigned n);

    private:
        /**
         * Computes the root of the nth power of real number
         * @param x
         * @param n
         * @return double
         */
        static double rt(double x, unsigned n);

    protected:
    };
}

#include <ostream>
#include "../../../../source/Complex.cpp"

#endif //CPP_OOP_1_COMPLEX_H
