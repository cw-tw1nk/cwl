/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_1_MATRIXT_H
#define CPP_OOP_1_MATRIXT_H


#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <cmath>
#include <cstring>

namespace cw {
    template<class T = double>
    class Matrix {
    protected:
//        size_t * rowWatcher = new size_t;
        size_t rows;
        size_t columns;
        T **array;

        virtual T **createArray2d(size_t rows, size_t columns) const final {
            if (rows * columns == 0) {
                return nullptr;
            }
            auto **array = new T *[rows];
            array[0] = new T[columns * rows];
            for (size_t i = 1; i < columns; ++i) {
                array[i] = array[0] + i * columns;
            }
            return array;
        }

    public:


        Matrix() : rows(0), columns(0) {
            rows = 0;
            columns = 0;
            array = nullptr;
        }

        Matrix(size_t rows, size_t columns) : rows(rows), columns(columns) {
            try {
                array = Matrix<T>::createArray2d(rows, columns);
            } catch (std::bad_alloc &e) {
                throw e;
            }
        }

        Matrix(const T **t, size_t rows, size_t columns) : rows(rows), columns(columns) {
            array = Matrix<T>::createArray2d(rows, columns);
            for (int i = 0; i < rows; ++i) {
                for (int j = 0; j < columns; ++j) {
                    array[i][j] = t[i][j];
                }
            }
        }

        Matrix(size_t Rows, size_t Columns, T object) : rows(Rows), columns(Columns) {
            try {
                array = Matrix<T>::createArray2d(Rows, Columns);
            } catch (std::bad_alloc &e) {
                throw e;
            }
            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Columns; ++j) {
                    array[i][j] = object;
                }
            }
        }

        Matrix(const Matrix<T> &other) : rows(other.rows), columns(other.columns) {
            if (rows == 0 or columns == 0) {
                array = nullptr;
            } else {
                try {
                    array = Matrix<T>::createArray2d(rows, columns);
                } catch (std::bad_alloc &e) {
                    throw e;
                }
                memcpy(array[0], other.array[0], sizeof(T) * rows * columns);
                for (size_t i = 1; i < rows; ++i) {
                    *(array + i) = *array + columns * i;
                }
            }
        }

        virtual ~Matrix() {
            delete[] array[0];
            delete[] array;
        }

        size_t getRows() const {
            return rows;
        }

        size_t getColumns() const {
            return columns;
        }

        T &operator()(size_t row, size_t column) const {
            if (row < 0 or column < 0) {
                throw std::out_of_range("Index can not be negative");
            } else if (row >= rows or column >= columns) {
                throw std::out_of_range("Index out of range");
            }
            return array[row][column];
        }

        const bool operator==(const Matrix<T> &matrixT) const {
            if (rows != matrixT.rows or columns != matrixT.columns) {
                return false;
            } else {
                for (size_t i = 0; i < rows; ++i) {
                    for (size_t j = 0; j < columns; ++j) {
                        if (array[i][j] != matrixT.array[i][j]) {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        const bool operator!=(const Matrix<T> &matrixT) const {
            return !(*this == matrixT);
        }

        Matrix<T> &operator=(const Matrix<T> &other) {
            if (*this == other) {
                return *this;
            } else if (rows == other.rows and columns == other.columns) {
                memcpy(array[0], other.array[0], sizeof(T) * rows * columns);
                for (size_t i = 1; i < rows; ++i) {
                    *(array + i) = *array + columns * i;
                }
            } else {
                delete[] array;
                rows = other.rows;
                columns = other.columns;
                array = Matrix<T>::createArray2d(rows, columns);
                memcpy(array[0], other.array[0], sizeof(T) * rows * columns);
                for (size_t i = 1; i < rows; ++i) {
                    *(array + i) = *array + columns * i;
                }

            }
            return *this;
        }

        template<class V>
        friend std::ostream &operator<<(std::ostream &os, const Matrix<V> &t) {
            os << "\n{" <<
               "\n\t\"name\":\"" << typeid(t).name() << "\"" <<
               "\n\t\"rows\": " << t.rows <<
               "\n\t\"columns\": " << t.columns <<
               "\n\t\"items\": [";
            for (size_t i = 0; i < t.rows; ++i) {
                os << "\n\t\t[\n\t\t\t";
                for (size_t j = 0; j < t.columns; ++j) {
                    os << t.array[i][j];
                    if (j != t.columns - 1)os << ", ";
                }
                os << "\n\t\t]";
                if (i != t.rows - 1)os << ',';
            }
            os << "\n\t]\n}\n";
            return os;
        }

        template<class V>
        friend std::istream &operator>>(std::istream &is, Matrix<V> &t) {
            for (size_t i = 0; i < t.rows; ++i) {
                for (size_t j = 0; j < t.columns; ++j) {
                    is >> t.array[i][j];
                }
            }
            return is;
        }

    protected:
        virtual void swapLine(T **array, size_t src, size_t dest) const final {
            T *temp = array[src];
            array[src] = array[dest];
            array[dest] = temp;
        }

    };
}


#endif //CPP_OOP_1_MATRIXT_H