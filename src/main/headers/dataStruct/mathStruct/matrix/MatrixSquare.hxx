/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_OOP_MATRIXSQUARE_H
#define CPP_OOP_MATRIXSQUARE_H

#include <dataStruct/mathStruct/matrix/MatrixRectangle.hxx>

namespace cw {

    template<class T = double>
    class MatrixSquare : public MatrixRectangle<T> {
    public:
        MatrixSquare();

        MatrixSquare(const T **t, size_t size);

        explicit MatrixSquare(size_t Rows);

        MatrixSquare(size_t Rows, T number);

        MatrixSquare(const MatrixSquare<T> &other);

        explicit MatrixSquare(const MatrixRectangle <T> &other);

        virtual ~MatrixSquare() = default;

        const MatrixSquare<T> inverse() const;


        const T determinant() const;

        MatrixSquare<T> &operator=(const MatrixSquare<T> &other);

        const MatrixSquare<T> operator/(const MatrixSquare<T> &other) const;

        const MatrixSquare<T> &operator/=(const MatrixSquare<T> &other);

        const T norm() const;

        template<class V>
        friend std::ostream &operator<<(std::ostream &os, const MatrixSquare<V> &t);
    };

    template<class V>
    std::ostream &operator<<(std::ostream &os, const MatrixSquare<V> &t) {
        os << "\n{" <<
           "\n\t\"name\":\"" << typeid(t).name() << "\"" <<
           "\n\t\"rows\": " << t.rows <<
           "\n\t\"columns\": " << t.columns <<
           "\n\t\"items\": [";
        for (size_t i = 0; i < t.rows; ++i) {
            os << "\n\t\t[\n\t\t\t";
            for (size_t j = 0; j < t.columns; ++j) {
                os << t.array[i][j];
                if (j != t.columns - 1)os << ", ";
            }
            os << "\n\t\t]";
            if (i != t.rows - 1)os << ',';
        }
        os << "\n\t]\n}\n";
        return os;
    }

    template<class T>
    const T MatrixSquare<T>::determinant() const {
        size_t watcher = 0;
        if (this->rows == 0) {
            throw std::domain_error("Degenerated matrix has no determinant");
        }
        auto **result = Matrix<T>::createArray2d(this->rows, this->columns);
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                result[i][j] = this->array[i][j];
            }
        }
        if (this->rows == 1) {
            return result[0][0];
        }
        T res = 1;
        for (size_t k = 0; k < this->rows; k++) {
            size_t count = 0;
            bool flag;
            do {
                if (result[k][k] == T()) {
                    if (count == this->rows - 1) return T();
                    this->swapLine(result, k, count + 1);
                    if (k == watcher) watcher = count + 1;
                    else if (count + 1 == watcher) watcher = k;
                    count++;
                    res *= -1;
                    flag = true;
                } else flag = false;
            } while (flag);
            for (size_t i = k + 1; i < this->rows; i++) {
                T b = result[i][k] / result[k][k];
                for (size_t j = k; j < this->rows; j++) {
                    result[i][j] -= result[k][j] * b;
                }
            }
        }
        for (size_t i = 0; i < this->rows; i++) {
            res *= result[i][i];
        }
        delete[] result[watcher];
        delete[] result;
        return res;
    }

    template<class T>
    const MatrixSquare<T> MatrixSquare<T>::inverse() const {
        T detT;
        try {
            detT = this->determinant();
        } catch (std::domain_error &e) {
            throw std::domain_error(e);
        }
        if (detT == T()) {
            throw std::domain_error("Determinant is Zero. Has no inverse.");
        }

        if (this->rows == 1) {
            return MatrixSquare<T>(1, this->array[0][0]);
        } else {

            MatrixSquare<T> inverse(this->rows, this->rows);
            for (size_t i = 0; i < this->rows; ++i) {
                for (size_t j = 0; j < this->rows; ++j) {
                    MatrixSquare<T> temp;
                    T temp1;
                    try {
                        temp = this->getMinor(i, j);
                        temp1 = temp.determinant();
                    } catch (std::domain_error &e) {
                        throw std::domain_error(e);
                    }
                    inverse.array[i][j] = temp1 * (((i + j) % 2 == 0) ? 1 : -1);
                }
            }
            return inverse.transpose() / this->determinant();
        }
    }

    template<class T>
    MatrixSquare<T> &MatrixSquare<T>::operator=(const MatrixSquare<T> &other) {
        if (*this == other) {
            return *this;
        } else if (this->rows == other.rows) {
            memcpy(this->array[0], other.array[0], sizeof(T) * this->rows * this->rows);
            for (size_t i = 1; i < this->rows; ++i) {
                *(this->array + i) = *(this->array) + this->rows * i;
            }
        } else {
            delete[] this->array;
            this->rows = other.rows;
            this->columns = other.rows;
            try {
                this->array = this->createArray2d(this->rows, this->rows);
            } catch (std::bad_alloc &e) {
                throw e;
            }
            memcpy(this->array[0], other.array[0], sizeof(T) * this->rows * this->rows);
            for (size_t i = 1; i < this->rows; ++i) {
                *(this->array + i) = *(this->array) + this->rows * i;
            }
        }
        return *this;
    }


    template<class T>
    const MatrixSquare<T> MatrixSquare<T>::operator/(const MatrixSquare<T> &other) const {
        MatrixSquare<T> inverseRight(other.rows, other.rows);
        MatrixSquare<T> result;
        try {
            inverseRight = other.inverse();
            result = *this * inverseRight;
        } catch (std::domain_error &e) {
            throw std::domain_error(e);
        }
        return result;
    }

    template<class T>
    const MatrixSquare<T> &MatrixSquare<T>::operator/=(const MatrixSquare<T> &other) {
        MatrixSquare<T> inverseRight(other.rows, other.rows);
        try {
            inverseRight = other.inverse();
            *this *= inverseRight;
        } catch (std::domain_error &e) {
            throw std::domain_error(e);
        }
        return *this;
    }

    template<class T>
    MatrixSquare<T>::MatrixSquare(size_t Rows) : MatrixRectangle<T>(Rows, Rows) {}

    template<class T>
    MatrixSquare<T>::MatrixSquare(size_t Rows, T number) : MatrixRectangle<T>(Rows, Rows, number) {}

    template<class T>
    MatrixSquare<T>::MatrixSquare(const MatrixSquare<T> &other) : MatrixRectangle<T>(other) {}

    template<class T>
    MatrixSquare<T>::MatrixSquare() : MatrixRectangle<T>() {}

    template<class T>
    MatrixSquare<T>::MatrixSquare(const MatrixRectangle <T> &other) {
        if (other.getRows() != other.getColumns()) {
            throw std::domain_error("Can not convert Rectangle matrix to Square matrix");
        } else {
            this->rows = other.getRows();
            this->columns = other.getColumns();
            this->array = this->createArray2d(this->rows, this->rows);
            for (size_t i = 1; i < this->rows; ++i) {
                *(this->array + i) = *(this->array) + this->rows * i;
            }

            for (size_t j = 0; j < this->rows; ++j) {
                for (size_t i = 0; i < this->columns; ++i) {
                    this->array[j][i] = other(j, i);
                }
            }
        }
    }

    template<class T>
    const T MatrixSquare<T>::norm() const {
        T result = 0;
        for (size_t i = 0; i < this->rows; ++i) {
            for (size_t j = 0; j < this->columns; ++j) {
                result += this->array[i][j] * this->array[i][j];
            }
        }
        return sqrt(result);
    }

    template<class T>
    MatrixSquare<T>::MatrixSquare(const T **t, size_t size): MatrixRectangle<T>(t, size, size) {}


}


#endif //CPP_OOP_MATRIXSQUARE_H
