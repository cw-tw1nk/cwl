/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CPP_OOP_1_A_H
#define CPP_OOP_1_A_H

class M {
private:
    int t = 0;

    class d {
        friend class B;

        M &b;

        d(M &b) : b(b) {};
    public:
        M &operator++() const {
            ++b.t;
            ++b.t;
            return b;
        }
    };

public:
    M() {

    }
//    const d operator++(int){
//        return d(*this);
//    }

    int getT() const {
        return t;
    }
};

#endif //CPP_OOP_1_A_H
