/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include "sandbox/I.h"
#include <sandbox/BBB.h>

I &AAA::operator+(I &i) {
    if (AAA *aaa = dynamic_cast<AAA *>(&i)) {
        std::cout << "aaa+aaa" << std::endl;
        this->test += aaa->test;
    } else if (BBB *bbb = dynamic_cast<BBB *>(&i)) {
        std::cout << "aaa+bbb" << std::endl;
        this->test += bbb->test;
    }
    return *this;
}

AAA::AAA(int test) : test(test) {

}

int AAA::getValue() const {
    return test;
}
