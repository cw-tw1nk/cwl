/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <complex/Complex.h>
#include <string>
#include <cmath>
#include <stdexcept>

namespace cw {
    Complex::Complex() : Real(0), Image(0) {}

    Complex::Complex(double Re, double Im) : Real(Re), Image(Im) {}


    std::ostream &operator<<(std::ostream &os, const Complex &complex) {
        os.precision(17);

        if (complex.Real != 0 && fabs(complex.Real) > 0.0001) os << complex.Real;
        if (complex.Image != 0 && fabs(complex.Image) > 0.0001) {
            if (complex.Image > 0 && (fabs(complex.Real) > 0.0001)) os << "+";
            os << complex.Image << "i";
        }
        if (fabs(complex.Real) < 0.0001 && fabs(complex.Image) < 0.0001) {
            os << 0;
        }
        return os;
    }

    std::istream &operator>>(std::istream &is, Complex &complex) {
        is.setf(std::ios_base::skipws);
        is >> complex.Real;
        is >> complex.Image;
        return is;
    }

    const Complex Complex::operator+(const Complex &c) const {
        return Complex(this->Real + c.Real, this->Image + c.Image);
    }

    const Complex Complex::operator-(const Complex &c) const {
        return Complex(this->Real - c.Real, this->Image - c.Image);
    }


    const Complex Complex::operator*(const Complex &c) const {
        return Complex(this->Real * c.Real - this->Image * c.Image, this->Real * c.Image + this->Image * c.Real);
    }

    const Complex Complex::operator/(const Complex &c) const {
        double re = (this->Real * c.Real + this->Image * c.Image) / (c.Image * c.Image + c.Real * c.Real);
        double im = (this->Image * c.Real - this->Real * c.Image) / (c.Image * c.Image + c.Real * c.Real);
        return Complex(re, im);
    }

    bool Complex::operator==(const Complex &rhs) const {
        return std::fabs(Real - rhs.Real) < 1e-8 &&
               std::fabs(Image - rhs.Image) < 1e-8;
    }

    bool Complex::operator!=(const Complex &rhs) const {
        return !(rhs == *this);
    }

    const Complex Complex::operator+(const double Re) const {
        return Complex(this->Real + Re, this->Image);
    }

    const Complex operator+(const double Re, const Complex &c) {
        return c.operator+(Re);
    }

    const Complex Complex::operator-(const double Re) const {
        return Complex(this->Real - Re, this->Image);
    }

    const Complex operator-(const double Re, const Complex &c) {
        return Complex(Re - c.Real, c.Image);
    }

    const Complex Complex::operator*(double Re) const {
        return Complex(this->Real * Re, this->Image * Re);
    }

    const Complex operator*(double Re, const Complex &c) {
        return c.operator*(Re);
    }

    const Complex Complex::operator/(double Re) const {
        return Complex(this->Real / Re, this->Image / Re);
    }

    const Complex operator/(double Re, const Complex &c) {
        return Complex(Re, 0) / c;
    }


    const double Complex::module() const {
        return std::sqrt(Real * Real + Image * Image);
    }

    const double Complex::angle() const {
        if (Real > 0 && Image >= 0)return std::atan(Image / Real);
        else if (Real < 0 && Image >= 0) return M_PI - atan(fabs(Image / Real));
        else if (Real < 0 && Image < 0) return M_PI + atan(fabs(Image / Real));
        else if (Real > 0 && Image < 0) return M_PI + M_PI - atan(fabs(Image / Real));
        else if (Real == 0 && Image > 0) return M_PI_2;
        else if (Real == 0 && Image < 0) return 3 * M_PI_2;
        throw std::exception();
    }

    double Complex::getRe() const {
        return Real;
    }

    void Complex::setRe(double Re) {
        this->Real = Re;
    }

    double Complex::getIm() const {
        return Image;
    }

    void Complex::setIm(double Im) {
        this->Image = Im;
    }

    const Complex Complex::outputTrigonometricForm() const {
        std::cout << module() << " ( cos( " << angle() << " )+i*sin( " << angle() << " ) )\n";
        return *this;
    }

    const Complex Complex::outputExponentialForm() const {
        std::cout << module() << "*e^" << angle() << "i\n";
        return *this;
    }

    Complex *Complex::root(unsigned n) const {
        if (n < 1) {
            throw std::domain_error("n is not a natural number");
        } else if (n == 1) {
            return new Complex(this->Real, this->Image);
        } else if (Image == 0) {
            if (Real == 1) {
                return new Complex(1, 0);
            } else if (n % 2 != 0) {
                return new Complex(rt(Real, n), 0);
            } else if (Real == 0) {
                return new Complex;
            } else if (Real > 0) {
                return new Complex(rt(Real, n), n);
            }
        }
        double m = module(),
                a = angle();
        double sqm = rt(m, n);
        auto *complex = new Complex[n];
        for (unsigned i = 0; i < n; i++) {
            double phi = (a + 2 * M_PI * i) / n;
            complex[i].setRe(sqm * cos(phi));
            complex[i].setIm(sqm * sin(phi));

        }
        return complex;
    }

    Complex *Complex::root(double a, int n) {

        if (n < 1) {
            throw std::domain_error("n is not a natural number");
        } else if (n == 1) {
            return new Complex(a, 0);
        } else if (a == 1) {
            return new Complex(1, 0);
        } else if (n % 2 != 0) {
            return new Complex(rt(a, n), 0);
        } else if (a == 0) {
            return new Complex;
        } else if (a > 0) {
            return new Complex(rt(a, n), n);
        }


        double angle = M_PI;
        double m = fabs(a);
        double sqm = rt(m, n);
        auto *complex = new Complex[n];
        for (int i = 0; i < n; i++) {
            double phi = (angle + 2 * M_PI * i) / n;
            complex[i].setRe(sqm * cos(phi));
            complex[i].setIm(sqm * sin(phi));
        }
        return complex;
    }

    double Complex::rt(double x, unsigned n) {

        double eps = 1e-5;
        double xk1 = x;
        double xk = 0;
        while (true) {
            xk = ((n - 1) * xk1 + x / pow(xk1, n - 1)) / n;
            if (fabs(xk - xk1) < eps) break;
            xk1 = xk;
        }
        return xk;
    }

    double Complex::pow(double a, unsigned n) {
        double an = 1;
        for (; n;) {
            if (n & 1) an *= a;
            a *= a;
            n >>= 1;
        }
        return an;
    }

    const Complex Complex::operator^(unsigned n) {
        if (n == 0)
            return Complex();
        else if (n == 1)
            return *this;
        double d = pow(module(), n);
        double x = n * angle();
        return Complex(d * cos(x), d * sin(x));
    }

    Complex &Complex::operator+=(const Complex &c) {
        Real += c.Real;
        Image += c.Image;
        return *this;
    }

    Complex &Complex::operator-=(const Complex &c) {
        Real -= c.Real;
        Image -= c.Image;
        return *this;
    }

    Complex &Complex::operator*=(const Complex &c) {
        double re = Real * c.Real - Image * c.Image;
        double im = Real * c.Image + Image * c.Real;
        Real = re;
        Image = im;
        return *this;
    }

    Complex &Complex::operator/=(const Complex &c) {
        double re = (this->Real * c.Real + this->Image * c.Image) / (c.Image * c.Image + c.Real * c.Real);
        double im = (this->Image * c.Real - this->Real * c.Image) / (c.Image * c.Image + c.Real * c.Real);
        Real = re;
        Image = im;
        return *this;
    }


    Complex &Complex::operator+=(double Re) {
        Real += Re;
        return *this;
    }

    Complex &Complex::operator-=(double Re) {
        Real -= Re;
        return *this;
    }

    Complex &Complex::operator*=(double Re) {
        Real *= Re;
        Image *= Re;
        return *this;
    }

    Complex &Complex::operator/=(double Re) {
        Real /= Re;
        Image /= Re;
        return *this;
    }

    const Complex &Complex::operator+() const {
        return *this;
    }

    Complex Complex::operator-() const {
        return Complex(-Real, -Image);
    }

    const Complex Complex::Ln(int n) const {
        double ro = std::log(Real * Real + Image * Image) / 2;
        return Complex(ro, angle() + 2 * M_PI * n);
    }

    const Complex Complex::operator^(const Complex &b) {
        return pow(b, 0);
    }

    const Complex Complex::pow(const Complex &b, int n) const {
        Complex c = b * Ln(n);
        return Complex(exp(c.Real) * cos(c.Image), exp(c.Real) * sin(c.Image));
    }

}