/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <gtest/gtest.h>
#include <dataStruct/TArray.hxx>

TEST(Array, equality) {
    cw::TArray<int> array;
    cw::TArray<int> array2;
    cw::TArray<int> array3(1);
    ASSERT_TRUE(array == array2);
    ASSERT_FALSE(array != array2);
    ASSERT_TRUE(array2 != array3);
}

TEST(Array, creation) {
    cw::TArray<int> array;
    ASSERT_TRUE(array.size() == 0);
    ASSERT_THROW(cw::TArray<int> array1(-10), std::bad_alloc);
}

TEST(Array, access) {
    cw::TArray<int> a(3, 0);
    ASSERT_EQ(a[0], 0);
    ASSERT_EQ(a[1], 0);
    ASSERT_EQ(a[2], 0);
    a[0] = 1;
    a[1] = 2;
    a[2] = 3;
    ASSERT_EQ(a[0], 1);
    ASSERT_EQ(a[1], 2);
    ASSERT_EQ(a[2], 3);
}
