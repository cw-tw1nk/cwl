/*
 *    Copyright © 2019 Allex Atamanenko. All rights reserved.
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include "dataStruct/mathStruct/complex/Complex.h"

using cw::Complex;

TEST(Complex, equality) {
    ASSERT_TRUE(Complex(1, 1) == Complex(1, 1));
    ASSERT_FALSE(Complex(1, 2) == Complex(1, 1));
    ASSERT_TRUE(Complex(1, 2) != Complex(1, 1));
    ASSERT_FALSE(Complex(1, 1) != Complex(1, 1));
    Complex complex(2, 1);
    ASSERT_TRUE(complex == complex);
    ASSERT_FALSE(complex != complex);
}

TEST(Complex, sign) {
    ASSERT_EQ(Complex(1, 1), +Complex(1, 1));
    ASSERT_EQ(Complex(-1, -1), -Complex(1, 1));
}

TEST(Complex, addition) {
    ASSERT_EQ(Complex(6, 5), Complex(2, 2) + Complex(4, 3));
    ASSERT_EQ(Complex(3, 2), Complex(2, 2) + 1);
    ASSERT_EQ(Complex(4, 5), 1 + Complex(3, 5));
    Complex complex(1, 2);
    complex += complex;
    ASSERT_EQ(Complex(2, 4), complex);
    complex += 2.5;
    ASSERT_EQ(Complex(4.5, 4), complex);

}

TEST(Complex, subtraction) {
    ASSERT_EQ(Complex(-2, -1), Complex(2, 2) - Complex(4, 3));
    ASSERT_EQ(Complex(-2, 2), Complex(2, 2) - 4);
    ASSERT_EQ(Complex(0, 3), 4 - Complex(4, 3));
    Complex complex(1, 2);
    Complex complex2(4, 8);
    complex -= complex2;
    ASSERT_EQ(Complex(-3, -6), complex);
    complex -= .5;
    ASSERT_EQ(Complex(-3.5, -6), complex);

}

TEST(Complex, multiplication) {
    ASSERT_EQ(Complex(-29, 31), (Complex(3, 5) * Complex(2, 7)));
    ASSERT_EQ(Complex(9, 15), Complex(3, 5) * 3);
    ASSERT_EQ(Complex(10, 35), 5 * Complex(2, 7));
    Complex complex(3, 5);
    complex *= complex;
    ASSERT_EQ(Complex(-16, 30), complex);
    complex *= 2;
    ASSERT_EQ(Complex(-32, 60), complex);

}

TEST(Complex, division) {
    const Complex &complex = Complex(10, 35) / Complex(2, 5);
    ASSERT_FLOAT_EQ(complex.getRe(), 6.7241378);
    ASSERT_FLOAT_EQ(complex.getIm(), 0.689655);
    ASSERT_EQ(Complex(2, 5), Complex(4, 10) / 2);
    ASSERT_EQ(Complex(0.8, -1.6), 8 / Complex(2, 4));
    Complex complex1(4, 6);
    Complex complex2(1, 2);
    complex1 /= complex2;
    ASSERT_EQ(Complex(3.2, -0.4), complex1);
    complex1 /= 4;
    ASSERT_EQ(Complex(0.8, -0.1), complex1);

}

TEST(Complex, angle) {
    ASSERT_FLOAT_EQ(0.463647609, Complex(2, 1).angle());
    ASSERT_FLOAT_EQ(0, Complex(2, 0).angle());
    ASSERT_FLOAT_EQ(3.60524026259, Complex(-2, -1).angle());
    ASSERT_FLOAT_EQ(3.14159265359, Complex(-2, 0).angle());
    ASSERT_FLOAT_EQ(5.81953769818, Complex(2, -1).angle());
    ASSERT_FLOAT_EQ(2.67794504459, Complex(-2, 1).angle());
    ASSERT_FLOAT_EQ(1.57079632679, Complex(0, 1).angle());
    ASSERT_FLOAT_EQ(4.71238898038, Complex(0, -1).angle());
}

TEST(Complex, module) {
    ASSERT_FLOAT_EQ(2.2360679775, Complex(2, -1).module());
    ASSERT_FLOAT_EQ(5, Complex(4, 3).module());
}

TEST(Complex, Ln) {
    Complex complex(1, 2);
    ASSERT_EQ(Complex(0.804718956, 1.10714872), complex.Ln(0));
}

TEST(Complex, power) {
    Complex complex(4, 5);
    ASSERT_EQ(Complex(0, 0), complex ^ 0);
    ASSERT_EQ(Complex(4, 5), complex ^ 1);
    const Complex &complex1 = complex ^2;
    ASSERT_FLOAT_EQ(-9, complex1.getRe());
    ASSERT_FLOAT_EQ(40, complex1.getIm());
    const Complex &complex2 = complex ^10;
    ASSERT_FLOAT_EQ(-103595049, complex2.getRe());
    ASSERT_FLOAT_EQ(51872200, complex2.getIm());
    Complex complex3(1, 2);
    const Complex &complex4 = Complex(3, 4);
    ASSERT_EQ(Complex(0.1290095940744668940770523396524472440918454644722947277,
                      0.03392409290517012669761785462254790154054732022267760839),
              complex3 ^ complex4);
}

TEST(Complex, root) {
    Complex complex(4, 5);
    auto *complexes = new Complex[10];
    complexes[0].setRe(1.1992047673846543);
    complexes[0].setIm(0.107743907463395);
    complexes[1].setRe(0.90684675671829);
    complexes[1].setIm(0.79204152892577);
    complexes[2].setRe(0.268104107573150);
    complexes[2].setIm(1.173806206839940);
    complexes[3].setRe(-0.47304519814148);
    complexes[3].setIm(1.107216809946839);
    complexes[4].setRe(-1.03350731638099);
    complexes[4].setIm(0.61770822456928);
    complexes[5].setRe(-1.19920476738465);
    complexes[5].setIm(-0.107743907463395);
    complexes[6].setRe(-0.90684675671829);
    complexes[6].setIm(-0.79204152892577);
    complexes[7].setRe(-0.26810410757315);
    complexes[7].setIm(-1.17380620683994);
    complexes[8].setRe(0.473045198141478);
    complexes[8].setIm(-1.10721680994684);
    complexes[9].setRe(1.033507316380990);
    complexes[9].setIm(-0.61770822456928);
    ASSERT_EQ(Complex(2.28069334, 1.09615789), complex.root(2)[0]);
    Complex *d = complex.root(10);
    for (int i = 0; i < 10; i++) {
        EXPECT_EQ(complexes[i], d[i]);
    }
}
